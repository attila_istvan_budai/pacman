import unittest
import ericsson.clients as client
import numpy as np

class MyTestCase(unittest.TestCase):
    def setUp(cls):
        client.Actions.possibleMovesCache = {}
        client.node_matrix = None
        client.all_distances = None
        client.all_paths = None
        client.walkable_node = None

    def test_mahnatten_distance(self):
        res = client.manhattanDistance((0,0), (1,1))
        self.assertEqual(2, res)

    def test_direction_to_vector(self):
        x,y = client.Actions.directionToVector(client.Direction.EAST)
        self.assertEqual(0, x)
        self.assertEqual(1, y)

    def test_generate_successor(self):
        agentPos = client.AgentPosition((1,1), client.Direction.NORTH, 5,5)
        res = agentPos.generateSuccessor((1,0))
        self.assertEqual((2,1), res.getPosition())

    def test_generate_successor_on_top(self):
        agentPos = client.AgentPosition((1,1), client.Direction.NORTH, 2,2)
        res = agentPos.generateSuccessor((0,1))
        self.assertEqual((1,0), res.getPosition())

    def test_getLegalActions_allway(self):
        map = client.Map(2,2,[['1','1'],['1','1']])
        game_state = client.GameState()
        pacmans = [client.PacmanFromInput(0, 'team', (1, 1), 0, 0, 'XXXX')]
        ghosts = []
        game_state.initialize(map, pacmans, ghosts)
        legal_actions = client.PacmanRules.getLegalActions(game_state, 0)
        print(legal_actions)
        self.assertEqual(4, len(legal_actions))

    def test_getLegalActions_noValidAction(self):
        map = client.Map(3, 3, [['F', 'F', 'F'], ['F', '1', 'F'], ['F', 'F', 'F']])
        game_state = client.GameState()
        pacmans = [client.PacmanFromInput(0, 'team', (1, 1), 0, 0, 'XXXX')]
        ghosts = []
        game_state.initialize(map, pacmans, ghosts)
        legal_actions = client.PacmanRules.getLegalActions(game_state, 0)
        self.assertEqual(0, len(legal_actions))

    def test_getLegalActions_twoValidActions(self):
        map = client.Map(3, 3, [['F', '1', 'F'], ['1', '1', 'F'], ['F', 'F', 'F']])
        game_state = client.GameState()
        pacmans = [client.PacmanFromInput(0, 'team', (1, 1), 0, 0, 'XXXX')]
        ghosts = []
        game_state.initialize(map, pacmans, ghosts)
        legal_actions = client.PacmanRules.getLegalActions(game_state, 0)
        self.assertEqual(2, len(legal_actions))
        self.assertEqual(True, '^' in legal_actions)
        self.assertEqual(True, '<' in legal_actions)

    def test_getLegalActions_oneValidActionsOnTheTop(self):
        map = client.Map(3, 3, [['F', '1', 'F'], ['1', ' ', 'F'], ['F', 'F', 'F']])
        game_state = client.GameState()
        pacmans = [client.PacmanFromInput(0, 'team', (0, 1), 0, 0, 'XXXX')]
        ghosts = []
        game_state.initialize(map, pacmans, ghosts)
        legal_actions = client.PacmanRules.getLegalActions(game_state, 0)
        for a in map.walls:
            print(a)
        print(legal_actions)
        self.assertEqual(1, len(legal_actions))
        self.assertEqual(True, 'v' in legal_actions)

    def test_applyAction(self):
        map = client.Map(3, 3, [['F', '1', 'F'], ['1', ' ', 'F'], ['F', 'F', 'F']])
        game_state = client.GameState()
        pacmans = [client.PacmanFromInput(0, 'team', (1, 1), 0, 0, 'XXXX')]
        ghosts = []
        game_state.initialize(map, pacmans, ghosts)
        client.PacmanRules.applyAction(game_state, client.Direction.NORTH, 0)
        self.assertEqual(1, game_state.getNumFood())

    def test_gamestate_generateSuccessor(self):
        map = client.Map(3, 3, [['F', '1', 'F'], ['1', '1', 'F'], ['F', 'F', 'F']])
        for a in map.walls:
            print(a)
        game_state = client.GameState()
        pacmans = [client.PacmanFromInput(0, 'team', (1, 1), 0, 0, 'XXXX')]
        ghosts = []
        game_state.initialize(map, pacmans, ghosts)
        next_state = game_state.generateSuccessor(0, client.Direction.NORTH)
        print(next_state.getPacmanState(0).getPosition())
        next_state = next_state.generateSuccessor(0, client.Direction.SOUTH)
        print(next_state.getPacmanState(0).getPosition())
        print(next_state.getScore())
        next_state = next_state.generateSuccessor(0, client.Direction.WEST)
        print(next_state.getScore())
        self.assertEqual(True, next_state.isWin())

    def test_gamestate_getCapsule(self):
        map = client.Map(3, 3, [['F', '+', 'F'], ['1', '1', 'F'], ['F', 'F', 'F']])
        for a in map.walls:
            print(a)
        game_state = client.GameState()
        pacmans = [client.PacmanFromInput(0, 'team', (1, 1), 0, 0, 'XXXX')]
        ghosts = []
        game_state.initialize(map, pacmans, ghosts)
        next_state = game_state.generateSuccessor(0, client.Direction.NORTH)
        print(next_state.getPacmanState(0).getPosition())

    def test_gamestate_eaten_by_ghost(self):
        map = client.Map(3, 3, [['F', '1', 'F'], ['1', '1', 'F'], ['F', 'F', 'F']])
        for a in map.walls:
            print(a)
        game_state = client.GameState()
        pacmans = [client.PacmanFromInput(0, 'team', (1, 1), 0, 0, 'XXXX')]
        ghosts = [client.GhostFromInput('a', (0, 1), 0, 4)]
        game_state.initialize(map, pacmans, ghosts)
        next_state = game_state.generateSuccessor(0, client.Direction.NORTH)
        print(next_state.getPacmanState(0).getPosition())
        print(next_state.getScore())
        print(next_state.getGhostPositions())
        self.assertEqual(True, next_state.isLose())

    def test_grapMatrix(self):
        map = client.Map(3, 3, [[' ', '+', 'F'], ['1', '1', 'F'], ['F', 'F', 'F']])
        for a in map.walls:
            print(a)
        map.generateGraphMatrix(map.nodes,map.height,map.width)
        print(map.nodes)
        print(client.all_distances)
        print(client.all_paths)

    def test_grapMatrix_overmap(self):
        map = client.Map(2, 3, [[' ', '+', ' '], ['F', 'F', 'F']])
        for a in map.walls:
            print(a)
        map.generateGraphMatrix(map.nodes,map.height,map.width)
        print(map.nodes)
        print(client.all_distances)
        print(client.all_paths)

    def test_manhattan_distance(self):
        map = client.Map(3, 4, [[' ', 'F', '1', 'F'], ['1', 'F', '1', 'F'], ['1', '1', '1', 'F']])

        map.generateGraphMatrix(map.nodes,map.height,map.width)
        print(client.all_distances)
        print(client.manhattanDistance((0,0), (2,0)))

    def test_generate_graph(self):
        layout = '''FFFFFFFFFFFFFFFFFFFFFFFFFF
F11111111111FF11111111111F
F1FFFF1FFFF1FF1FFFF1FFFF1F
F+FFFF1FFFF1FF1FFFF1FFFF+F
F1FFFF1FFFF1FF1FFFF1FFFF1F
F111111111111111111111111F
F1FFFF1F1FFFFFFFF1F1FFFF1F
F1FFFF1F1FFFFFFFF1F1FFFF1F
F111111F1111FF1111F111111F
FFFFFF1FFFF FF FFFF1FFFFFF
FFFFFF1FF        FF1FFFFFF
FFFFFF1FF FFGGFF FF1FFFFFF
      1   F    F   1      
FFFFFF1FF FFFFFF FF1FFFFFF
F11111111111FF11111111111F
F1FFFF1FFFF1FF1FFFF1FFFF1F
F1FFFF1FFFF1FF1FFFF1FFFF1F
F+11FF111111  111111FF11+F
FFF1FF1F1FFFFFFFF1F1FF1FFF
FFF1FF1F1FFFFFFFF1F1FF1FFF
F111111F1111FF1111F111111F
F1FFFFFFFFF1FF1FFFFFFFFF1F
F1FFFFFFFFF1FF1FFFFFFFFF1F
11111111111111111111111111
FFFFFFFFFFFFFFFFFFFFFFFFFF'''
        fields =[]
        for row in layout.splitlines():
            fields.append(row)
        map = client.Map(25, 26, fields)
        map.generateGraphMatrix(map.nodes,map.height,map.width)
        np.set_printoptions(threshold=99999)
        print(client.walkable_node)
        client.manhattanDistance((17,13), (12,13))
        client.manhattanDistance((17, 13), (23, 0))
        client.manhattanDistance((17, 13), (3, 24))
        path = client.shortestPath((17, 13), (3, 24))
        first = client.shortestPathDirection((17, 13), (3, 24))
        print('shortestPath')
        print(path)
        print(first)

        print(len(client.decision_ponts))
        print(client.decision_ponts)



    def test_checklegalmoves(self):
        layout = '''FFFFFFFFFFFFFFFFFFFFFFFFFF
F11111111111FF11111111111F
F1FFFF1FFFF1FF1FFFF1FFFF1F
F+FFFF1FFFF1FF1FFFF1FFFF+F
F1FFFF1FFFF1FF1FFFF1FFFF1F
F111111111111111111111111F
F1FFFF1F1FFFFFFFF1F1FFFF1F
F1FFFF1F1FFFFFFFF1F1FFFF1F
F111111F1111FF1111F111111F
FFFFFF1FFFF FF FFFF1FFFFFF
FFFFFF1FF        FF1FFFFFF
FFFFFF1FF FFGGFF FF1FFFFFF
      1   F    F   1      
FFFFFF1FF FFFFFF FF1FFFFFF
F11111111111FF11111111111F
F1FFFF1FFFF1FF1FFFF1FFFF1F
F1FFFF1FFFF1FF1FFFF1FFFF1F
F+11FF111111  111111FF11+F
FFF1FF1F1FFFFFFFF1F1FF1FFF
FFF1FF1F1FFFFFFFF1F1FF1FFF
F111111F1111FF1111F111111F
F1FFFFFFFFF1FF1FFFFFFFFF1F
F1FFFFFFFFF1FF1FFFFFFFFF1F
11111111111111111111111111
FFFFFFFFFFFFFFFFFFFFFFFFFF'''
        fields =[]
        for row in layout.splitlines():
            fields.append(row)
        map = client.Map(25, 26, fields)

        game_state = client.GameState()
        pacmans = [client.PacmanFromInput(0, 'team', (14, 17), 0, 0, 'XXXX')]
        ghosts = [client.GhostFromInput('a', (12, 12), 0, 4)]
        game_state.initialize(map, pacmans, ghosts)
        print(map.walls[14][17])
        print(map.walls[13][17])
        print(map.walls[15][17])
        print(map.walls[14][16])
        print(map.walls[14][18])

        legal_actions = client.PacmanRules.getLegalActions(game_state, 0)
        print(legal_actions)
        self.assertEqual(2, len(legal_actions))


    def test_closest_ghost_positions(self):
        agent = client.EAgent()
        curPos = (1,1)






if __name__ == '__main__':
    unittest.main()
