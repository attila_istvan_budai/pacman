import sys
import random


import sys
import copy
from collections import defaultdict
import numpy as np
from scipy.sparse.csgraph import floyd_warshall
from collections import namedtuple



def isPacmanAgent(agentId):
    return isinstance(agentId, int)


class Grid:
    def __init__(self, width, height, initialValue=False, bitRepresentation=None):
        self.width = width
        self.height = height
        self.data = np.full((height, width), initialValue, dtype=bool)

    def __getitem__(self, i):
        return self.data[i]

    def __setitem__(self, key, item):
        self.data[key] = item

    def __str__(self):
        return self.data

    def __eq__(self, other):
        if other == None: return False
        return self.data == other.data

    def __hash__(self):
        # return hash(str(self))
        base = 1
        h = 0
        for l in self.data:
            for i in l:
                if i:
                    h += base
                base *= 2
        return hash(h)

    def copy(self):
        g = Grid(self.width, self.height)
        g.data = np.copy(self.data)
        return g

    def deepCopy(self):
        return self.copy()

    def shallowCopy(self):
        g = Grid(self.width, self.height)
        g.data = [x[:] for x in self.data]
        return g

    def count(self, item =True ):
        return np.count_nonzero(self.data == item)

    def asList(self, key = True):
        list = []
        for x in range(self.width):
            for y in range(self.height):
                if self[y][x] == key: list.append( (y,x) )
        return list

class AgentPosition:

    def __init__(self, pos, direction,width,height):
        self.pos = pos
        self.direction = direction
        self.width = width
        self.height = height

    def getPosition(self):
        return (self.pos)

    def getDirection(self):
        return self.direction

    def __eq__(self, other):
        if other == None: return False
        return (self.pos == other.pos and self.direction == other.direction)

    def __hash__(self):
        x = hash(self.pos)
        y = hash(self.direction)
        return hash(x + 13 * y)

    def __str__(self):
        return "(x,y)="+str(self.pos)+", "+str(self.direction)

    def generateSuccessor(self, vector):
        x, y= self.pos
        dx, dy = vector
        direction = Actions.vectorToDirection(vector)
        return AgentPosition(((x + dx)%self.width, (y+dy)%self.height), direction, self.height, self.width)

class PacmanState:
    def __init__(self, startPosition, agentId, fastTime):
        self.agentId = agentId
        self.start = startPosition
        self.configuration = startPosition
        self.fastTime = fastTime

    def __str__(self):
        return "Pacman " + str(self.configuration)

    def __eq__( self, other ):
        if other == None:
            return False
        return self.configuration == other.configuration

    def __hash__(self):
        return hash(hash(self.configuration))

    def copy( self ):
        state = PacmanState( self.start, self.agentId, self.fastTime )
        state.configuration = self.configuration
        state.fastTime = self.fastTime
        return state

    def getPosition(self):
        if self.configuration == None: return None
        return self.configuration.getPosition()

    def getDirection(self):
        return self.configuration.getDirection()

    def getFastTime(self):
        return self.fastTime

class GhostState:
    def __init__(self, startPosition, agentId, eatable):
        self.agentId = agentId
        self.start = startPosition
        self.configuration = startPosition
        self.frozen_time = 0
        self.eatable = eatable

    def __str__(self):
        return "Ghost " + str(self.configuration)

    def __eq__( self, other ):
        if other == None:
            return False
        return self.configuration == other.configuration and self.eatable == other.eatable and self.frozen_time == other.frozen_time

    def __hash__(self):
        return hash(hash(self.configuration) + 13 * hash(self.eatable))

    def copy( self ):
        state = PacmanState( self.start )
        state.configuration = self.configuration
        state.frozen_time = self.frozen_time
        state.eatable = self.eatable
        return state

    def getPosition(self):
        if self.configuration == None: return None
        return self.configuration.getPosition()

    def getDirection(self):
        return self.configuration.getDirection()

class Direction:
    NORTH = '^'
    SOUTH = 'v'
    EAST = '>'
    WEST = '<'

class Actions:
    _directions = {Direction.NORTH: (-1, 0),
                   Direction.SOUTH: (1, 0),
                   Direction.EAST:  (0, 1),
                   Direction.WEST:  (0, -1)}

    _directionsAsList = _directions.items()

    possibleMovesCache = {}

    def directionToVector(direction, speed = 1.0):

        dx, dy =  Actions._directions[direction]
        return (dx * speed, dy * speed)
    directionToVector = staticmethod(directionToVector)

    def vectorToDirection(vector):
        dy, dx = vector
        if dy > 0:
            return Direction.NORTH
        if dy < 0:
            return Direction.SOUTH
        if dx > 0:
            return Direction.WEST
        return Direction.EAST
    vectorToDirection = staticmethod(vectorToDirection)

    def getPossibleActions(config, walls):
        possible = []
        x, y = config.pos

        if (x, y) in Actions.possibleMovesCache:
            return Actions.possibleMovesCache[(x,y)].copy()

        for dir, vec in Actions._directionsAsList:
            dx, dy = vec
            next_x = int(x + dx)
            next_y = int(y + dy)

            walls_width = walls.width
            walls_height = walls.height
            if not walls[next_x % walls_height][next_y % walls_width]:
                possible.append(dir)
        Actions.possibleMovesCache[(x, y)] = possible.copy()
        return possible
    getPossibleActions = staticmethod(getPossibleActions)

    def getLegalNeighbors(position, walls):
        x,y = position
        x_int, y_int = int(x + 0.5), int(y + 0.5)
        neighbors = []
        for dir, vec in Actions._directionsAsList:
            dx, dy = vec
            next_x = x_int + dx
            if next_x < 0 or next_x == walls.width: continue
            next_y = y_int + dy
            if next_y < 0 or next_y == walls.height: continue
            if not walls[next_x][next_y]: neighbors.append((next_x, next_y))
        return neighbors
    getLegalNeighbors = staticmethod(getLegalNeighbors)

    def getSuccessor(position, action):
        dx, dy = Actions.directionToVector(action)
        x, y = position
        return (x + dx, y + dy)
    getSuccessor = staticmethod(getSuccessor)

class GameStateData:
    def __init__( self, prevState = None ):
        if prevState != None:
            self.food = prevState.food.shallowCopy()
            self.capsules = prevState.capsules[:]
            self.pacmanAgentStates = self.copyAgentStates( prevState.pacmanAgentStates )
            self.ghostAgentStates = self.copyAgentStates( prevState.ghostAgentStates )
            self.map = prevState.map
            self.deadPacman = prevState.deadPacman
            self.deadGhost = prevState.deadGhost
            self.score = prevState.score

        self._foodEaten = None
        self._foodAdded = None
        self._capsuleEaten = None
        self._agentMoved = None
        self._lose = False
        self._win = False
        self.scoreChange = 0

    def deepCopy( self ):
        state = GameStateData( self )
        state.food = self.food.deepCopy()
        state.map = self.map.deepCopy()
        state._agentMoved = self._agentMoved
        state._foodEaten = self._foodEaten
        state._foodAdded = self._foodAdded
        state._capsuleEaten = self._capsuleEaten
        return state

    def copyAgentStates( self, agentStates ):
        return copy.deepcopy(agentStates)

    def __eq__( self, other ):
        """
        Allows two states to be compared.
        """
        if other == None: return False
        # TODO Check for type of other
        if not self.pacmanAgentStates == other.pacmanAgentStates: return False
        if not self.ghostAgentStates == other.ghostAgentStates: return False
        if not np.array_equal(self.food, other.food): return False
        if not self.capsules == other.capsules: return False
        if not self.score == other.score: return False
        return True

    def __hash__( self ):
        """
        Allows states to be keys of dictionaries.
        """
        for i, state in enumerate( self.agentStates ):
            try:
                int(hash(state))
            except TypeError as e:
                print(e)
                #hash(state)
        return int((hash(tuple(self.agentStates)) + 13*hash(self.food) + 113* hash(tuple(self.capsules)) + 7 * hash(self.score)) % 1048575 )

    def initialize( self, map, pacmanIds, pacmanPositions, ghostPositions, fastTime):
        self.food = map.food.copy()
        self.capsules = map.capsules[:]
        self.map = map
        self.score = 0
        self.scoreChange = 0

        self.pacmanAgentStates = {}
        self.ghostAgentStates = {}

        for i in range(0, len(pacmanPositions)):
            pos = pacmanPositions[i]
            id = pacmanIds[i]
            self.pacmanAgentStates[id] = PacmanState(AgentPosition(pos, Direction.NORTH, map.width, map.height), id, fastTime)
        for id, ghostPosition in ghostPositions.items():
            pos = ghostPosition[0]
            self.ghostAgentStates[id] = GhostState(AgentPosition(pos, Direction.NORTH, map.width, map.height), id, ghostPosition[1])
        self.deadPacman = [False for a in self.pacmanAgentStates]
        self.deadGhost = [False for a in self.ghostAgentStates]

TIME_PENALTY = 1

class GameState:
    def getLegalActions( self, agentIndex=0 ):
        if self.isWin() or self.isLose(): return []

        if isPacmanAgent(agentIndex):  # Pacman is moving
            return PacmanRules.getLegalActions( self, agentIndex )
        else:
            return GhostRules.getLegalActions( self, agentIndex )

    def generateSuccessor( self, agentIndex, action):
        if self.isWin() or self.isLose(): raise Exception('Can\'t generate a successor of a terminal state.')

        state = GameState(self)

        if isPacmanAgent(agentIndex):  # Pacman is moving
            state.data.dead = [False for i in range(state.getNumAgents())]
            PacmanRules.applyAction( state, action, agentIndex )
        else:                # A ghost is moving
            GhostRules.applyAction( state, action, agentIndex )

        # Time passes
        if agentIndex == 0:
            state.data.scoreChange += -TIME_PENALTY # Penalty for waiting around
        else:
            GhostRules.decrementTimer( state.data.ghostAgentStates[agentIndex] )

        # Resolve multi-agent effects
        GhostRules.checkDeath( state, agentIndex )

        state.data._agentMoved = agentIndex
        state.data.score += state.data.scoreChange
        return state

    def getLegalPacmanActions( self ):
        return self.getLegalActions( 0 )

    def generatePacmanSuccessor( self, action ):
        return self.generateSuccessor( 0, action )

    def getPacmanState( self, agentIndex ):
        return self.data.pacmanAgentStates[agentIndex].copy()

    def getPacmanStates( self ):
        return self.data.pacmanAgentStates

    def getPacmanPosition( self, agentIndex=0 ):
        return self.data.pacmanAgentStates[agentIndex].getPosition()

    def getGhostStates( self ):
        return self.data.ghostAgentStates

    def getGhostState( self, agentIndex ):
        return self.data.ghostAgentStates[agentIndex]

    def getGhostPosition( self, agentIndex ):
        if agentIndex == 0:
            raise Exception("Pacman's index passed to getGhostPosition")
        return self.data.ghostAgentStates[agentIndex].getPosition()

    def getGhostPositions(self):
        return [s.getPosition() for s in self.getGhostStates().values()]

    def getNumAgents( self ):
        return len( self.data.pacmanAgentStates) + len(self.data.ghostAgentStates )

    def getScore( self ):
        return float(self.data.score)

    def getCapsules(self):
        return self.data.capsules

    def getNumFood( self ):
        return self.data.food.count()

    def getFood(self):
        return self.data.food

    def getWalls(self):
        return self.data.map.walls

    def getMapWidth(self):
        return self.data.map.width

    def getMapHeight(self):
        return self.data.map.height

    def hasFood(self, x, y):
        return self.data.food[x][y]

    def hasWall(self, x, y):
        return self.data.map.walls[x][y]

    def isLose( self ):
        return self.data._lose

    def isWin( self ):
        return self.data._win

    def __init__( self, prevState = None ):
        if prevState != None: # Initial state
            self.data = GameStateData(prevState.data)
        else:
            self.data = GameStateData()

    def deepCopy( self ):
        state = GameState( self )
        state.data = self.data.deepCopy()
        return state

    def __eq__( self, other ):
        return hasattr(other, 'data') and self.data == other.data

    def __hash__( self ):
        return hash( self.data )

    def __str__( self ):
        return str(self.data)

    def initialize( self, layout, pacmanIds, pacmanPositions, ghostPositions, fastTime):
        self.data.initialize(layout, pacmanIds, pacmanPositions, ghostPositions, fastTime)

SCARED_TIME=21

class PacmanRules:
    PACMAN_SPEED=1


    def getLegalActions( state, agentIndex ):
        return Actions.getPossibleActions( state.getPacmanState(agentIndex).configuration, state.data.map.walls )
    getLegalActions = staticmethod( getLegalActions )

    def applyAction( state, action, agentIndex ):
        """
        Edits the state to reflect the results of the action.
        """
        legal = PacmanRules.getLegalActions( state, agentIndex )
        if action not in legal:
            raise Exception("Illegal action " + action)

        pacmanState = state.data.pacmanAgentStates[agentIndex]

        # Update Configuration
        vector = Actions.directionToVector( action, PacmanRules.PACMAN_SPEED )
        pacmanState.configuration = pacmanState.configuration.generateSuccessor( vector )

        # Eat
        next = pacmanState.configuration.getPosition()
        PacmanRules.consume( next, state )
    applyAction = staticmethod( applyAction )

    def consume( position, state ):
        x,y = position
        width = state.data.food.width
        height = state.data.food.height
        x = x % width
        y = y % height
        # Eat food
        if state.data.food[x][y]:
            state.data.scoreChange += 10
            state.data.food = state.data.food.copy()
            state.data.food[x][y] = False
            state.data._foodEaten = position
            numFood = state.getNumFood()
            if numFood == 0 and not state.data._lose:
                state.data.scoreChange += 500
                state.data._win = True
        # Eat capsule
        if( position in state.getCapsules() ):
            state.data.capsules.remove( position )
            state.data._capsuleEaten = position
            state.data.scoreChange += 50
            # Reset all ghosts' scared timers
            for id, ghostState in state.data.ghostAgentStates.items():
                ghostState.scaredTimer = SCARED_TIME
    consume = staticmethod( consume )

class GhostRules:
    """
    These functions dictate how ghosts interact with their environment.
    """
    GHOST_SPEED=1.0
    def getLegalActions( state, ghostIndex ):
        conf = state.getGhostState( ghostIndex ).configuration
        possibleActions = Actions.getPossibleActions( conf, state.data.map.walls )
        return possibleActions
    getLegalActions = staticmethod( getLegalActions )

    def applyAction( state, action, ghostIndex):

        legal = GhostRules.getLegalActions( state, ghostIndex )
        if action not in legal:
            raise Exception("Illegal ghost action " + str(action))

        ghostState = state.data.ghostAgentStates[ghostIndex]
        speed = GhostRules.GHOST_SPEED
        if ghostState.eatable > 0: speed /= 2.0
        vector = Actions.directionToVector( action, speed )
        ghostState.configuration = ghostState.configuration.generateSuccessor( vector )
    applyAction = staticmethod( applyAction )

    def decrementTimer( ghostState):
        timer = ghostState.eatable
        if timer == 1:
            ghostState.configuration.pos = ghostState.configuration.pos
        ghostState.eatable = max( 0, timer - 1 )
    decrementTimer = staticmethod( decrementTimer )

    def checkDeath( state, agentIndex):
        if isPacmanAgent(agentIndex):
            pacmanPosition = state.getPacmanPosition(agentIndex)
            for key, ghostState in state.getGhostStates().items():
                ghostPosition = ghostState.configuration.getPosition()
                if ( pacmanPosition == ghostPosition ):
                    GhostRules.collide( state, ghostState, key )
        else:
            ghostPosition = state.getGhostPosition(agentIndex)
            ghostState = state.getGhostState(agentIndex)
            for id, pacmanState in state.getPacmanStates().items():
                pacmanPosition = pacmanState.configuration.getPosition()
                if ( pacmanPosition == ghostPosition ):
                    GhostRules.collide( state, ghostState, pacmanState.agentId )
    checkDeath = staticmethod( checkDeath )

    def collide( state, ghostState, agentIndex):
        if ghostState.eatable > 0:
            state.data.scoreChange += 200
            GhostRules.placeGhost(state, ghostState)
            ghostState.eatable = 0
            # Added for first-person
            #state.data.deadGhost[agentIndex] = True
        else:
            if not state.data._win:
                state.data.scoreChange -= 500
                state.data._lose = True
    collide = staticmethod( collide )

    def placeGhost(state, ghostState):
        ghostState.configuration = ghostState.start
    placeGhost = staticmethod( placeGhost )


node_matrix = None
all_distances = None
all_paths = None
walkable_node = None
decision_ponts = None

def shortestPath( xy1, xy2 ):
    path = []
    if walkable_node is not None:
        n2 = walkable_node.index(xy1)
        n1 = walkable_node.index(xy2)
        while(all_paths[n1][n2]!=-9999):
            nextPost = walkable_node[n2]
            n2 = all_paths[n1][n2]
            path.append(nextPost)
        path.append(xy2)
        return path
    return None

def shortestPathDirection( xy1, xy2 ):
    path = []
    if walkable_node is not None:
        n2 = walkable_node.index(xy1)
        n1 = walkable_node.index(xy2)
        n2 = all_paths[n1][n2]
        return walkable_node[n2]
    return None

def manhattanDistance( xy1, xy2 ):
    if walkable_node is not None:
        n1 = walkable_node.index(xy1)
        n2 = walkable_node.index(xy2)
        return all_distances[n1][n2]

    "Returns the Manhattan distance between points xy1 and xy2"
    return abs( xy1[0] - xy2[0] ) + abs( xy1[1] - xy2[1] )

class Map:
    def __init__(self, height, width, fields):
        self.height = height
        self.width = width
        self.food = Grid(width,height)
        self.walls = Grid(width,height)
        self.capsules = []
        self.nodes = []
        for y in range( self.height ):
            for x in range(self.width):
                layoutChar = fields[y][x]
                if layoutChar == 'F':
                    self.walls[y][x] = True
                elif layoutChar == '1':
                    self.food[y][x] = True
                    self.nodes.append((y, x))
                elif layoutChar == '+':
                    self.capsules.append((y, x))
                    self.nodes.append((y, x))
                elif layoutChar == ' ':
                    self.nodes.append((y, x))
                elif layoutChar == 'G':
                    self.nodes.append((y, x))
        if node_matrix is None:
            global walkable_node
            walkable_node = self.nodes
            self.generateGraphMatrix(self.nodes,height,width)

    def generateGraphMatrix(self, nodes,height, width):
        size = len(nodes)
        global node_matrix
        node_matrix = np.full((size, size), 0, dtype=int)
        for i in range(len(nodes)):
            node = nodes[i]
            x = node[0]
            y = node[1]
            up = ((x-1)%width, y)
            down = ((x + 1) % width, y)
            left = (x, (y-1)%height)
            right = (x, (y + 1) % height)
            dirs = [up, down, left, right]
            for dir in dirs:
                if dir in nodes:
                    n = nodes.index(dir)
                    node_matrix[i][n] = 1
                    node_matrix[n][i] = 1
        #could be nices
        global all_distances
        global all_paths
        all_distances, all_paths  = floyd_warshall(node_matrix, directed=False, unweighted=True, overwrite=False, return_predecessors=True)
        self.generateDecisionPoints(nodes, node_matrix)

    def generateDecisionPoints(self, nodes, node_matrix):
        rows = (node_matrix != 0).sum(1)
        indexes = [i for i, val in enumerate(rows) if val > 2]
        global decision_ponts
        decision_ponts = [nodes[i] for i in indexes]


DistanceWithPosition = namedtuple('DistanceWithPosition', ['distance', 'position'])
GhostTuple = namedtuple('GhostTuple', ['distance', 'eatable','position'])
ScoreActionState = namedtuple('ScoreActionState', ['score', 'action','state'])

class EAgent:


    def __init__(self):
        self.ghostPosHistory = defaultdict(list)
        self.pacManPosHistory = defaultdict(list)
        self.gameLogCache = []

    def getPacmanHistory(self, agentIndex=0):
        return self.pacManPosHistory[0]

    def closest_dot_distances(self, cur_pos, food_pos):
        food_distances = []
        for food in food_pos:
            food_distances.append(manhattanDistance(food, cur_pos))
        food_distances.sort()
        return food_distances

    def closest_dot_distances_with_food_pos(self, cur_pos, food_pos):
        food_distances = []
        for food in food_pos:
            food_distances.append(DistanceWithPosition(distance=manhattanDistance(food, cur_pos), position=food))
        food_distances.sort()
        return food_distances

    def closest_ghost_positions(self, cur_pos, ghosts):
        ghost_distances = []
        for key, ghost in ghosts.items():
            ghost_distances.append(GhostTuple(distance=manhattanDistance(ghost.getPosition(), cur_pos), eatable=ghost.eatable,position=ghost.getPosition()))
        ghost_distances.sort(key=lambda tup: tup[0])
        return ghost_distances

    def food_stuff(self, cur_pos, food_positions):
        food_distances = []
        for food in food_positions:
            food_distances.append(manhattanDistance(food, cur_pos))
        return sum(food_distances)

    def cloeset_capsules(self, cur_pos, capsule_positions):
        cap_distances = []
        for cap in capsule_positions:
            cap_distances.append(manhattanDistance(cap, cur_pos))
        return min(cap_distances) if len(cap_distances) > 0 else 1


    def tunedEvaulationFunction(self,currentGameState, action):


        map_width = currentGameState.getMapWidth()
        map_height = currentGameState.getMapHeight()
        pacman_pos = currentGameState.getPacmanPosition()

        score = currentGameState.getScore()
        food = currentGameState.getFood().asList()
        ghosts = currentGameState.getGhostStates()
        capsules = currentGameState.getCapsules()

        closest_ghosts = self.closest_ghost_positions(pacman_pos, ghosts)
        food_stuff_score = self.food_stuff(pacman_pos, food)
        closest_capsule = self.cloeset_capsules(pacman_pos, capsules)

        fast_time = currentGameState.getPacmanState(0).getFastTime()[0]

        closest_dot_dist = self.closest_dot_distances(pacman_pos, food)
        ghost_positions = currentGameState.getGhostPositions()

        ghostScore = 0.0
        delta = 0.9


        for i in range(len(ghosts)):
            ghostScore+=float(closest_ghosts[i].distance)*delta
            delta*=0.9
        #food_stuff_score / len(food)
        penaltyForTheSameAction = 0
        #checking last move
        if len(self.getPacmanHistory())>2 and pacman_pos == self.getPacmanHistory()[-2]:
            penaltyForTheSameAction = 2


        #check if ghost in the path:
        first_closest_dot_dist = closest_dot_dist[0]
        second_closest_dot_dist=0
        if len(closest_dot_dist)>1:
            second_closest_dot_dist = closest_dot_dist[1]
        closest_dot_positions = self.closest_dot_distances_with_food_pos(pacman_pos, food)
        pathToFirstClosestFood = shortestPath(pacman_pos, closest_dot_positions[0].position)
        for pos in ghost_positions:
            if pos in pathToFirstClosestFood:
                first_closest_dot_dist = 0
        if len(closest_dot_positions)>1:
            pathToSecondClosestFood = shortestPath(pacman_pos, closest_dot_positions[1].position)
            for pos in ghost_positions:
                if pos in pathToSecondClosestFood:
                    second_closest_dot_dist = 0

        if fast_time>0:
            sc = score + float(ghostScore - first_closest_dot_dist - second_closest_dot_dist - penaltyForTheSameAction) / float(map_width + map_height)
        else:
            sc = score + float(ghostScore - closest_capsule * 2 - first_closest_dot_dist - second_closest_dot_dist - penaltyForTheSameAction) / float(map_width + map_height)

        self.gameLogCache.append("closest_capsule = {} closest dot 1 {} closest dot 2 {}\n".format(closest_capsule, first_closest_dot_dist, second_closest_dot_dist))
        if(closest_ghosts[0].distance<=1):
            sys.stderr.write("closest ghosts => %s \n" % str(closest_ghosts))
            if closest_ghosts[0].eatable==0:
                sc-=490
        self.gameLogCache.append("{6} [score = {0} gamescore = {1} ghost_score = {2} food = {3} closest_dots = {4} closest_capsule = {5}]\n".format(sc, score, ghostScore, food_stuff_score/ len(food), closest_dot_positions[0].distance, closest_capsule, action))
        return sc

    def read_data(self):
        # stdin-t olvassuk :)
        first_line_data = list(map(int, sys.stdin.readline().strip().split(" ")))
        second_line_data = sys.stdin.readline().strip().split(" ", 4)


        if len(second_line_data) > 4:
            sys.stderr.write("\nGot message: %s\n" % second_line_data[4])
        else:
            height, width, agentNumber, numGhosts = map(int, second_line_data)

        # TODO azert ennel kenyelmesebben is el lehet menteni az adatokat :)
        fields = []
        for i in range(height):
            fields.append(list(sys.stdin.readline())[:width])

        pacman_info = []
        for i in range(agentNumber):
            pacman_info.append(sys.stdin.readline().strip().split(" "))

        ghost_info = []
        for i in range(numGhosts):
            ghost_info.append(sys.stdin.readline().strip().split(" "))

        self.map = Map(height, width, fields)

        return (first_line_data, fields, pacman_info, ghost_info)

    def doActions(self, gameState, data):
        # Generate candidate actions
        legal = gameState.getLegalPacmanActions()
        fast_time = gameState.getPacmanState(0).getFastTime()
        pacman_pos = gameState.getPacmanPosition()

        closestGhost = self.closest_ghost_positions(pacman_pos, gameState.getGhostStates())[0]
        #HUNTING TIME
        #sys.stderr.write("closestGhosts %s\n" % str(closestGhost))
        #sys.stderr.write("fasttime %d eatable %d\n" % (fast_time[0], closestGhost[1]))
        if fast_time[0] > 0 and closestGhost[1]>0:
            pathToGhost = shortestPath(pacman_pos, closestGhost[2])
            self.gameLogCache.append("pathToGhost: %s\n" % (str(pathToGhost)))
            first_vec = (pacman_pos[0] - pathToGhost[1][0], pacman_pos[1] - pathToGhost[1][1])
            act1 = Actions.vectorToDirection(first_vec)
            if(len(pathToGhost)>2):
                sys.stderr.write("first_vec: %s\n" % (str(first_vec)))
                second_vec = (pathToGhost[1][0] - pathToGhost[2][0], pathToGhost[1][1] - pathToGhost[2][1])
                act2 = Actions.vectorToDirection(second_vec)
                sys.stderr.write("second_vec: %s\n" % (str(second_vec)))
                sys.stderr.write("%s %s %s %c %c\n" % (data[0], data[1], data[2], act1, act2))
                sys.stdout.write("%s %s %s %c %c\n" % (data[0], data[1], data[2], act1, act2))
            else:
                sys.stdout.write("%s %s %s %c\n" % (data[0], data[1], data[2], act1))
        elif fast_time[0] > 0:
            first_step = self.getActionFromSuccessor(gameState, legal)
            after_first_state = first_step.state
            legal2 = after_first_state.getLegalPacmanActions()
            second_step = self.getActionFromSuccessor(after_first_state, legal2)
            sys.stdout.write("%s %s %s %c %c\n" % (data[0], data[1], data[2], first_step.action,second_step.action))
        else:
            score_action_state = self.getActionFromSuccessor(gameState, legal)
            sys.stderr.write("%s %s %s %c\n" % (data[0], data[1], data[2], score_action_state.action))
            sys.stdout.write("%s %s %s %c\n" % (data[0], data[1], data[2], score_action_state.action))

    def getActionFromSuccessor(self, gameState, legal):
        successors = [(gameState.generateSuccessor(0, action), action) for action in legal]
        scored = [ScoreActionState(score=self.tunedEvaulationFunction(state, action), action=action, state=state) for state, action in successors]
        bestScore = max(scored).score
        bestActions = [tup for tup in scored if tup[0] == bestScore]
        return random.choice(bestActions)

    def actionLoop(self):
        while True:
            try:
                data, fields, pacmans, ghosts = self.read_data()
            except:
                for log in reversed(self.gameLogCache):
                    sys.stderr.write(log)

            game_state = GameState()
            pacmanIds = [int(pacman[0]) for pacman in pacmans]
            pacmanPos = [(int(pacman[2]),int(pacman[3])) for pacman in pacmans]

            for i in range(len(pacmanIds)):
                self.pacManPosHistory[pacmanIds[i]].append(pacmanPos[i])


            ghostDict = {ghost[0] : ((int(ghost[1]),int(ghost[2])),int(ghost[3])) for ghost in ghosts}
            for id, value in ghostDict.items():
                self.ghostPosHistory[id].append(value[0])
            fastTimes =  [int(pacman[4]) for pacman in pacmans]

            #sys.stderr.write("round %d move: %d %d\n" % (data[1], pacmanPos[0][0], pacmanPos[0][1]))
            self.gameLogCache.append("***round %d move: %d %d***\n" % (data[1], pacmanPos[0][0], pacmanPos[0][1]))

            game_state.initialize(self.map, pacmanIds, pacmanPos, ghostDict, fastTimes)


            if data[2] == -1:
               break
            self.doActions(game_state, data)


if __name__ == '__main__':
    agent = EAgent()
    agent.actionLoop()
