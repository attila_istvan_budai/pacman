from game.pacman import *
import sys

if __name__ == '__main__':
    """
    The main function called when pacman.py is run
    from the command line:

    > python pacman.py

    See the usage string for more details.

    > python pacman.py --help
        python pacman.py
        python pacman.py  -p GreedyAgent
        python pacman.py -p LeftTurnAgent
        python pacman.py -p GreedyAgent -l testClassic
        python pacman.py -p LeftTurnAgent -l testClassic
        python pacman.py --frameTime 0 -p GreedyAgent -k 1
        python pacman.py --frameTime 0 -p ReflexAgent -k 2
        python pacman.py -p ReflexAgent -l openClassic -n 10 -q
        python pacman.py -p MinimaxAgent -l minimaxClassic -a depth=4
        python pacman.py -p MinimaxAgent -l trappedClassic -a depth=3
        python pacman.py -p AlphaBetaAgent -a depth=3 -l smallClassic
        python pacman.py -p AlphaBetaAgent -l trappedClassic -a depth=3 -q -n 10
        python pacman.py -p ExpectimaxAgent -l trappedClassic -a depth=3 -q -n 10
        python pacman.py -l smallClassic -p ExpectimaxAgent -a evalFn=better -q -n 10
        python pacman.py -l contestClassic -p ContestAgent -g DirectionalGhost -q -n 10
    """
    import time
    start = time.time()

    args = readCommand( sys.argv[1:] ) # Get game components based on input
    runGames( **args )

    end = time.time()
    print(end - start)

    # import cProfile
    # cProfile.run("runGames( **args )")
    pass