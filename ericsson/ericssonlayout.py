from util import manhattanDistance
from game.gameengine import Grid
import os
import random
from functools import reduce

class Map:

    def __init__(self, layoutText):
        self.height, self.width, self.agentNumber, self.numGhosts = map(int, layoutText[0].split(" "))
        self.walls = Grid(self.width, self.height, False)
        self.food = Grid(self.width, self.height, False)
        self.capsules = []
        self.agentPositions = []
        self.freezeTimes = []
        self.processLayoutText(layoutText)
        self.layoutText = layoutText
        self.totalFood = len(self.food.asList())

    def getNumGhosts(self):
        return self.numGhosts

    def isWall(self, pos):
        x, col = pos
        return self.walls[x][col]

    def __str__(self):
        return "\n".join(self.layoutText)

    def deepCopy(self):
        return Map(self.layoutText[:])

    def processLayoutText(self, layoutText):
        """
        Coordinates are flipped from the input format to the (x,y) convention here

        The shape of the maze.  Each character
        represents a different type of object.
         F - Wall
         . - Food
         o - Capsule
        Other characters are ignored.
        """
        maxY = self.height+1
        rows = []
        for y in range(1,self.height+1):
            tempRow = ''
            for x in range(self.width):
                if x>=len(layoutText[maxY-y]):
                    layoutChar = ' '
                else:
                    layoutChar = layoutText[maxY-y][x]
                self.processLayoutChar(x, y-1, layoutChar)
                tempRow += layoutChar
            rows.append(tempRow)
        for i in range(1+self.height, 1+self.height+self.agentNumber):
            data = layoutText[i].split(" ")
            self.agentPositions.append((0, (int(data[3]), maxY-2-int(data[2]))))
            self.freezeTimes.append(0)
        cnt = 1
        for i in range(1+self.height+self.agentNumber, 1+self.height+self.agentNumber+self.numGhosts):
            data = layoutText[i].split(" ")
            self.agentPositions.append((data[0], (int(data[2]), maxY-2-int(data[1]))))
            self.freezeTimes.append(int(data[4]))
            cnt+=1
        #self.agentPositions.sort()
        self.agentPositions = [ ( i == 0, pos) for i, pos in self.agentPositions]

    def processLayoutChar(self, x, y, layoutChar):
        if layoutChar == 'F':
            self.walls[x][y] = True
        elif layoutChar == '1':
            self.food[x][y] = True
        elif layoutChar == '+':
            self.capsules.append((x, y))

def getLayout(name, back = 2):
    if name.endswith('.lay'):
        layout = tryToLoad('ericssonlayout/' + name)
        if layout == None: layout = tryToLoad(name)
    else:
        layout = tryToLoad('ericssonlayout/' + name + '.lay')
        if layout == None: layout = tryToLoad(name + '.lay')
    if layout == None and back >= 0:
        curdir = os.path.abspath('.')
        os.chdir('..')
        layout = getLayout(name, back -1)
        os.chdir(curdir)
    return layout

def tryToLoad(fullname):
    if(not os.path.exists(fullname)): return None
    f = open(fullname)
    try: return Map([line for line in f])
    finally: f.close()
