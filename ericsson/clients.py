import sys
import random
import numba
from numba import jit

import sys
import copy
from collections import defaultdict
import numpy as np
from scipy.sparse.csgraph import floyd_warshall
from scipy.sparse.csgraph import shortest_path
from collections import namedtuple
import math

from multiprocessing.pool import ThreadPool


DistanceWithPosition = namedtuple('DistanceWithPosition', ['distance', 'position'])
DistancePositionPath = namedtuple('DistancePositionPath', ['distance', 'position', 'path'])
GhostTuple = namedtuple('GhostTuple', ['agentIndex', 'distance', 'eatable','position'])
ScoreActionState = namedtuple('ScoreActionState', ['score', 'action','state'])

GhostFromInput = namedtuple('GhostFromInput', ['id', 'position','eatable', 'stayingtime'])
PacmanFromInput = namedtuple('PacmanFromInput', ['id', 'teamname', 'position','fasttime', 'score', 'extrapoints'])
OutputAction = namedtuple('OutputAction', ['gameid', 'tick','pacmanid', 'dir1', 'dir2'])

InputGameData = namedtuple('InputGameData', ['gameid', 'tick','pacmanid'])
InputMapData = namedtuple('InputMapData', ['height', 'width','num_pacman', 'num_ghost'])

def isPacmanAgent(agentId):
    return isinstance(agentId, int)


class Grid:
    def __init__(self, width, height, initialValue=False, bitRepresentation=None):
        self.width = width
        self.height = height
        self.data = np.full((height, width), initialValue, dtype=bool)

    def __getitem__(self, i):
        return self.data[i]

    def __setitem__(self, key, item):
        self.data[key] = item

    def __str__(self):
        return self.data

    def __eq__(self, other):
        if other == None: return False
        return self.data == other.data

    def __hash__(self):
        # return hash(str(self))
        base = 1
        h = 0
        for l in self.data:
            for i in l:
                if i:
                    h += base
                base *= 2
        return hash(h)

    def copy(self):
        g = Grid(self.width, self.height)
        g.data = np.copy(self.data)
        return g

    def deepCopy(self):
        return self.copy()

    def shallowCopy(self):
        g = Grid(self.width, self.height)
        g.data = [x[:] for x in self.data]
        return g

    def count(self, item =True ):
        return np.count_nonzero(self.data == item)

    def asList(self, key = True):
        return [(y,x) for x in range(self.width) for y in range(self.height) if self[y][x] == key]

class AgentPosition:

    def __init__(self, pos, direction,width,height):
        self.pos = pos
        self.direction = direction
        self.width = width
        self.height = height

    def getPosition(self):
        return (self.pos)

    def getDirection(self):
        return self.direction

    def __eq__(self, other):
        if other == None: return False
        return (self.pos == other.pos and self.direction == other.direction)

    def __hash__(self):
        x = hash(self.pos)
        y = hash(self.direction)
        return hash(x + 13 * y)

    def __str__(self):
        return "(x,y)="+str(self.pos)+", "+str(self.direction)

    def generateSuccessor(self, vector):
        x, y= self.pos
        dx, dy = vector
        direction = Actions.vectorToDirection(vector)
        return AgentPosition(((x + dx)%self.height, (y+dy)%self.width), direction, self.height, self.width)

class PacmanState:
    def __init__(self, startPosition, agentId, fastTime):
        self.agentId = agentId
        self.start = startPosition
        self.configuration = startPosition
        self.fastTime = fastTime

    def __str__(self):
        return "Pacman " + str(self.configuration)

    def __eq__( self, other ):
        if other == None:
            return False
        return self.configuration == other.configuration

    def __hash__(self):
        return hash(hash(self.configuration))

    def copy( self ):
        state = PacmanState( self.start, self.agentId, self.fastTime )
        state.configuration = self.configuration
        state.fastTime = self.fastTime
        return state

    def getPosition(self):
        if self.configuration == None: return None
        return self.configuration.getPosition()

    def getDirection(self):
        return self.configuration.getDirection()

    def getFastTime(self):
        return self.fastTime

class GhostState:
    def __init__(self, startPosition, agentId, eatable, frozen_time):
        self.agentId = agentId
        self.start = startPosition
        self.configuration = startPosition
        self.frozen_time = frozen_time
        self.eatable = eatable

    def __str__(self):
        return "Ghost " + str(self.configuration)

    def __eq__( self, other ):
        if other == None:
            return False
        return self.configuration == other.configuration and self.eatable == other.eatable and self.frozen_time == other.frozen_time

    def __hash__(self):
        return hash(hash(self.configuration) + 13 * hash(self.eatable))

    def copy( self ):
        state = PacmanState( self.start )
        state.configuration = self.configuration
        state.frozen_time = self.frozen_time
        state.eatable = self.eatable
        return state

    def getPosition(self):
        if self.configuration == None: return None
        return self.configuration.getPosition()

    def getDirection(self):
        return self.configuration.getDirection()

class Direction:
    NORTH = '^'
    SOUTH = 'v'
    EAST = '>'
    WEST = '<'

class Actions:
    _directions = {Direction.NORTH: (-1, 0),
                   Direction.SOUTH: (1, 0),
                   Direction.EAST:  (0, 1),
                   Direction.WEST:  (0, -1)}

    _directionsAsList = _directions.items()

    possibleMovesCache = {}

    def directionToVector(direction, speed = 1.0):

        dx, dy =  Actions._directions[direction]
        return (dx * speed, dy * speed)
    directionToVector = staticmethod(directionToVector)

    def vectorToDirection(vector):
        dy, dx = vector
        if dy > 0:
            return Direction.NORTH
        if dy < 0:
            return Direction.SOUTH
        if dx > 0:
            return Direction.WEST
        return Direction.EAST
    vectorToDirection = staticmethod(vectorToDirection)

    def getPossibleActions(position, walls):
        possible = []
        x, y = position

        if (x, y) in Actions.possibleMovesCache:
            return Actions.possibleMovesCache[(x,y)].copy()

        for dir, vec in Actions._directionsAsList:
            dx, dy = vec
            next_x = int(x + dx)
            next_y = int(y + dy)

            walls_width = walls.width
            walls_height = walls.height
            if not walls[next_x % walls_height][next_y % walls_width]:
                possible.append(dir)
        Actions.possibleMovesCache[(x, y)] = possible.copy()
        return possible
    getPossibleActions = staticmethod(getPossibleActions)

    def getLegalNeighbors(position, walls):
        x,y = position
        x_int, y_int = int(x + 0.5), int(y + 0.5)
        neighbors = set()

        walls_width = walls.width
        walls_height = walls.height

        for dir, vec in Actions._directionsAsList:
            dx, dy = vec
            next_x = x_int + dx
            if next_x < 0 or next_x == walls.width: continue
            next_y = y_int + dy
            if next_y < 0 or next_y == walls.height: continue
            if not walls[next_x % walls_height][next_y % walls_width]: neighbors.add((next_x % walls_height, next_y % walls_width))
        return neighbors
    getLegalNeighbors = staticmethod(getLegalNeighbors)

    def getSuccessor(position, action):
        dx, dy = Actions.directionToVector(action)
        x, y = position
        return (x + dx, y + dy)
    getSuccessor = staticmethod(getSuccessor)

class GameStateData:
    def __init__( self, prevState = None ):
        if prevState != None:
            self.food = prevState.food.shallowCopy()
            self.capsules = prevState.capsules[:]
            self.pacmanAgentStates = self.copyAgentStates( prevState.pacmanAgentStates )
            self.ghostAgentStates = self.copyAgentStates( prevState.ghostAgentStates )
            self.map = prevState.map
            self.deadPacman = prevState.deadPacman
            self.deadGhost = prevState.deadGhost
            self.score = prevState.score
            self.tick = prevState.tick+1

        self._foodEaten = None
        self._foodAdded = None
        self._capsuleEaten = None
        self._agentMoved = None
        self._lose = False
        self._win = False
        self.scoreChange = 0

    def deepCopy( self ):
        state = GameStateData( self )
        state.food = self.food.deepCopy()
        state.map = self.map.deepCopy()
        state._agentMoved = self._agentMoved
        state._foodEaten = self._foodEaten
        state._foodAdded = self._foodAdded
        state._capsuleEaten = self._capsuleEaten
        state.tick = self.tick
        return state

    def copyAgentStates( self, agentStates ):
        return copy.deepcopy(agentStates)

    def __eq__( self, other ):
        """
        Allows two states to be compared.
        """
        if other == None: return False
        # TODO Check for type of other
        if not self.pacmanAgentStates == other.pacmanAgentStates: return False
        if not self.ghostAgentStates == other.ghostAgentStates: return False
        if not np.array_equal(self.food, other.food): return False
        if not self.capsules == other.capsules: return False
        if not self.score == other.score: return False
        return True

    def __hash__( self ):
        """
        Allows states to be keys of dictionaries.
        """
        for i, state in enumerate( self.agentStates ):
            try:
                int(hash(state))
            except TypeError as e:
                print(e)
                #hash(state)
        return int((hash(tuple(self.agentStates)) + 13*hash(self.food) + 113* hash(tuple(self.capsules)) + 7 * hash(self.score)) % 1048575 )

    def initialize( self, map, pacmans, ghosts, tick):
        self.food = map.food.copy()
        self.capsules = map.capsules[:]
        self.map = map
        self.score = 0
        self.scoreChange = 0

        self.pacmanAgentStates = {}
        self.ghostAgentStates = {}

        self.tick = tick

        for pacman in pacmans:
            self.pacmanAgentStates[pacman.id] = PacmanState(AgentPosition(pacman.position, Direction.NORTH, map.width, map.height), pacman.id,pacman.fasttime)
        for ghost in ghosts:
            self.ghostAgentStates[ghost.id] = GhostState(AgentPosition(ghost.position, Direction.NORTH, map.width, map.height), ghost.position, ghost.eatable, ghost.stayingtime)
        self.deadPacman = [False for a in self.pacmanAgentStates]
        self.deadGhost = [False for a in self.ghostAgentStates]

TIME_PENALTY = 1

class GameState:
    def getLegalActions( self, agentIndex=0 ):
        if self.isWin() or self.isLose(): return []

        if isPacmanAgent(agentIndex):  # Pacman is moving
            return PacmanRules.getLegalActions( self, agentIndex )
        else:
            return GhostRules.getLegalActions( self, agentIndex )

    def generateSuccessor( self, agentIndex, action):
        if self.isWin() or self.isLose(): raise Exception('Can\'t generate a successor of a terminal state.')

        state = GameState(self)

        if isPacmanAgent(agentIndex):  # Pacman is moving
            state.data.dead = [False for i in range(state.getNumAgents())]
            PacmanRules.applyAction( state, action, agentIndex )
        else:                # A ghost is moving
            GhostRules.applyAction( state, action, agentIndex )

        # Time passes
        if agentIndex == 0:
            state.data.scoreChange += -TIME_PENALTY # Penalty for waiting around
        else:
            GhostRules.decrementTimer( state.data.ghostAgentStates[agentIndex] )

        # Resolve multi-agent effects
        GhostRules.checkDeath( state, agentIndex )

        state.data._agentMoved = agentIndex
        state.data.score += state.data.scoreChange
        return state

    def getCurrentTick(self):
        return self.data.tick

    def getLegalPacmanActions( self ):
        return self.getLegalActions( 0 )

    def generatePacmanSuccessor( self, action ):
        return self.generateSuccessor( 0, action )

    def getPacmanState( self, agentIndex ):
        return self.data.pacmanAgentStates[agentIndex].copy()

    def getPacmanStates( self ):
        return self.data.pacmanAgentStates

    def getPacmanPosition( self, agentIndex=0 ):
        return self.data.pacmanAgentStates[agentIndex].getPosition()

    def getGhostStates( self ):
        return self.data.ghostAgentStates

    def getGhostState( self, agentIndex ):
        return self.data.ghostAgentStates[agentIndex]

    def getGhostPosition( self, agentIndex ):
        if agentIndex == 0:
            raise Exception("Pacman's index passed to getGhostPosition")
        return self.data.ghostAgentStates[agentIndex].getPosition()

    def getGhostPositions(self):
        return [s.getPosition() for s in self.getGhostStates().values()]

    def getNumAgents( self ):
        return len( self.data.pacmanAgentStates) + len(self.data.ghostAgentStates )

    def getScore( self ):
        return float(self.data.score)

    def getCapsules(self):
        return self.data.capsules

    def getNumFood( self ):
        return self.data.food.count()

    def getFood(self):
        return self.data.food

    def getWalls(self):
        return self.data.map.walls

    def getMapWidth(self):
        return self.data.map.width

    def getMapHeight(self):
        return self.data.map.height

    def hasFood(self, x, y):
        return self.data.food[x][y]

    def hasWall(self, x, y):
        return self.data.map.walls[x][y]

    def isLose( self ):
        return self.data._lose

    def isWin( self ):
        return self.data._win

    def __init__( self, prevState = None ):
        if prevState != None: # Initial state
            self.data = GameStateData(prevState.data)
        else:
            self.data = GameStateData()

    def deepCopy( self ):
        state = GameState( self )
        state.data = self.data.deepCopy()
        return state

    def __eq__( self, other ):
        return hasattr(other, 'data') and self.data == other.data

    def __hash__( self ):
        return hash( self.data )

    def __str__( self ):
        return str(self.data)

    def initialize( self, layout, pacmans, ghosts, tick):
        self.data.initialize(layout, pacmans, ghosts, tick)

SCARED_TIME=21

class PacmanRules:
    PACMAN_SPEED=1


    def getLegalActions( state, agentIndex ):
        return Actions.getPossibleActions( state.getPacmanState(agentIndex).configuration.pos, state.data.map.pacmanwalls )
    getLegalActions = staticmethod( getLegalActions )

    def applyAction( state, action, agentIndex ):
        """
        Edits the state to reflect the results of the action.
        """
        legal = PacmanRules.getLegalActions( state, agentIndex )
        if action not in legal:
            raise Exception("Illegal action " + action)

        pacmanState = state.data.pacmanAgentStates[agentIndex]

        # Update Configuration
        vector = Actions.directionToVector( action, PacmanRules.PACMAN_SPEED )
        pacmanState.configuration = pacmanState.configuration.generateSuccessor( vector )

        # Eat
        next = pacmanState.configuration.getPosition()
        PacmanRules.consume( next, state )
    applyAction = staticmethod( applyAction )

    def consume( position, state ):
        x,y = position
        width = state.data.food.width
        height = state.data.food.height
        x = x % width
        y = y % height
        # Eat food
        if state.data.food[x][y]:
            state.data.scoreChange += 10
            state.data.food = state.data.food.copy()
            state.data.food[x][y] = False
            state.data._foodEaten = position
            numFood = state.getNumFood()
            if numFood == 0 and not state.data._lose:
                state.data.scoreChange += 500
                state.data._win = True
        # Eat capsule
        if( position in state.getCapsules() ):
            state.data.capsules.remove( position )
            state.data._capsuleEaten = position
            state.data.scoreChange += 50
            # Reset all ghosts' scared timers
            for id, ghostState in state.data.ghostAgentStates.items():
                ghostState.scaredTimer = SCARED_TIME
    consume = staticmethod( consume )

class GhostRules:
    """
    These functions dictate how ghosts interact with their environment.
    """
    GHOST_SPEED=1.0
    def getLegalActions( state, ghostIndex ):
        conf = state.getGhostState( ghostIndex ).configuration
        possibleActions = Actions.getPossibleActions( conf.pos, state.data.map.walls )
        return possibleActions
    getLegalActions = staticmethod( getLegalActions )

    def applyAction( state, action, ghostIndex):

        legal = GhostRules.getLegalActions( state, ghostIndex )
        if action not in legal:
            raise Exception("Illegal ghost action " + str(action))

        ghostState = state.data.ghostAgentStates[ghostIndex]
        speed = GhostRules.GHOST_SPEED
        if ghostState.eatable > 0: speed /= 2.0
        vector = Actions.directionToVector( action, speed )
        ghostState.configuration = ghostState.configuration.generateSuccessor( vector )
    applyAction = staticmethod( applyAction )

    def decrementTimer( ghostState):
        timer = ghostState.eatable
        if timer == 1:
            ghostState.configuration.pos = ghostState.configuration.pos
        ghostState.eatable = max( 0, timer - 1 )
    decrementTimer = staticmethod( decrementTimer )

    def checkDeath( state, agentIndex):
        if isPacmanAgent(agentIndex):
            pacmanPosition = state.getPacmanPosition(agentIndex)
            for key, ghostState in state.getGhostStates().items():
                ghostPosition = ghostState.configuration.getPosition()
                if ( pacmanPosition == ghostPosition ):
                    GhostRules.collide( state, ghostState, key )
        else:
            ghostPosition = state.getGhostPosition(agentIndex)
            ghostState = state.getGhostState(agentIndex)
            for id, pacmanState in state.getPacmanStates().items():
                pacmanPosition = pacmanState.configuration.getPosition()
                if ( pacmanPosition == ghostPosition ):
                    GhostRules.collide( state, ghostState, pacmanState.agentId )
    checkDeath = staticmethod( checkDeath )

    def collide( state, ghostState, agentIndex):
        if ghostState.eatable > 0:
            state.data.scoreChange += 200
            GhostRules.placeGhost(state, ghostState)
            ghostState.eatable = 0
            # Added for first-person
            #state.data.deadGhost[agentIndex] = True
        else:
            if not state.data._win:
                state.data.scoreChange -= 500
                state.data._lose = True
    collide = staticmethod( collide )

    def placeGhost(state, ghostState):
        ghostState.configuration = ghostState.start
    placeGhost = staticmethod( placeGhost )


node_matrix = None
all_distances = None
all_paths = None
walkable_node = None
decision_points = None
ghost_shortest_pathes = dict()

def shortestPath( xy1, xy2 ):
    path = []
    if walkable_node is not None:
        n2 = walkable_node.index(xy1)
        n1 = walkable_node.index(xy2)
        while(all_paths[n1][n2]!=-9999):
            nextPost = walkable_node[n2]
            n2 = all_paths[n1][n2]
            path.append(nextPost)
        path.append(xy2)
        return path
    return None

def shortestPathDirection( xy1, xy2 ):
    path = []
    if walkable_node is not None:
        n2 = walkable_node.index(xy1)
        n1 = walkable_node.index(xy2)
        n2 = all_paths[n1][n2]
        return walkable_node[n2]
    return None


MY_INFINITY = 99999
def manhattanDistance( xy1, xy2 ):
    if walkable_node is not None:
        n1 = walkable_node.index(xy1)
        n2 = walkable_node.index(xy2)
        if math.isinf(all_distances[n1][n2]):
            return MY_INFINITY
        return all_distances[n1][n2]

    "Returns the Manhattan distance between points xy1 and xy2"
    return abs( xy1[0] - xy2[0] ) + abs( xy1[1] - xy2[1] )

def getGhostShortestDistance(prevNode, currentNode, targetNode):
    global ghost_shortest_pathes
    targetIndex = walkable_node.index(targetNode)
    currentIndex = walkable_node.index(currentNode)
    if (prevNode, currentNode) in ghost_shortest_pathes:
        dist_array = ghost_shortest_pathes[(prevNode, currentNode)]
        if math.isinf(dist_array[currentIndex][targetIndex]):
            dist_array[currentIndex][targetIndex] = manhattanDistance(currentNode, targetNode)
        return dist_array[currentIndex][targetIndex]
    else:
        my_node_matrix = node_matrix.copy()
        i1 = walkable_node.index(prevNode)

        my_node_matrix[i1][currentIndex] = 0
        my_node_matrix[currentIndex][i1] = 0

        dist_array = shortest_path(my_node_matrix)
        ghost_shortest_pathes[(prevNode, currentNode)] = dist_array

        if math.isinf(dist_array[currentIndex][targetIndex]):
            dist_array[currentIndex][targetIndex] = manhattanDistance(currentNode, targetNode)

        return dist_array[currentIndex][targetIndex]

class Map:
    def __init__(self, height, width, fields):
        self.height = height
        self.width = width
        self.food = Grid(width,height)
        self.walls = Grid(width,height)
        self.pacmanwalls = Grid(width, height)
        self.capsules = []
        self.nodes = []
        for y in range( self.height ):
            for x in range(self.width):
                layoutChar = fields[y][x]
                if layoutChar == 'F':
                    self.walls[y][x] = True
                    self.pacmanwalls[y][x] = True
                #elif layoutChar == 'G':
                #    self.pacmanwalls[y][x] = True
                elif layoutChar == '1':
                    self.food[y][x] = True
                    self.nodes.append((y, x))
                elif layoutChar == '+':
                    self.capsules.append((y, x))
                    self.nodes.append((y, x))
                elif layoutChar == ' ':
                    self.nodes.append((y, x))
                elif layoutChar == 'G':
                    self.pacmanwalls[y][x] = True
                    self.nodes.append((y, x))
        if node_matrix is None:
            global walkable_node
            walkable_node = self.nodes
            self.generateGraphMatrix(self.nodes,height,width)

    def generateGraphMatrix(self, nodes,height, width):
        size = len(nodes)
        global node_matrix
        node_matrix = np.full((size, size), 0, dtype=int)
        for i in range(len(nodes)):
            node = nodes[i]
            x = node[0]
            y = node[1]
            up = ((x-1)%width, y)
            down = ((x + 1) % width, y)
            left = (x, (y-1)%height)
            right = (x, (y + 1) % height)
            dirs = [up, down, left, right]
            for dir in dirs:
                if dir in nodes:
                    n = nodes.index(dir)
                    node_matrix[i][n] = 1
                    node_matrix[n][i] = 1
        #could be nices
        global all_distances
        global all_paths
        all_distances, all_paths  = floyd_warshall(node_matrix, directed=False, unweighted=True, overwrite=False, return_predecessors=True)
        self.generateDecisionPoints(nodes, node_matrix)
        #self.generateDecisionGraph()



    def generateDecisionPoints(self, nodes, node_matrix):
        rows = (node_matrix != 0).sum(1)
        indexes = [i for i, val in enumerate(rows) if val > 2]
        global decision_points
        decision_points = set([nodes[i] for i in indexes])
        decision_points_list = [nodes[i] for i in indexes]



class EAgent:


    def __init__(self):
        self.ghostPosHistory = defaultdict(list)
        self.pacManPosHistory = defaultdict(list)
        self.gameLogCache = []
        self.sameActionInRow = 0
        self.depth = 1

    def calculateGhostDistance(self, agentIndex, pacmanPos):
        if len(self.ghostPosHistory[agentIndex])<2:
            return manhattanDistance(self.ghostPosHistory[agentIndex][-1], pacmanPos)

        path = self.calculateMandatoryGhostPath(agentIndex)

        if pacmanPos in path:
            return path.index(pacmanPos)+1
        else:
            if len(path)>1:
                return len(path) + getGhostShortestDistance(path[-2], path[-1], pacmanPos)
            else:
                currentGhostPos = self.ghostPosHistory[agentIndex][-1]
                previousGhostPos = self.ghostPosHistory[agentIndex][-2]
                return getGhostShortestDistance(previousGhostPos, currentGhostPos, pacmanPos)


    def calculateMandatoryGhostPath(self, agentIndex):

        if len(self.ghostPosHistory[agentIndex])<2:
            return []

        expectedPath = []
        currentGhostPos = self.ghostPosHistory[agentIndex][-1]
        previousGhostPos = self.ghostPosHistory[agentIndex][-2]

        visited = set([currentGhostPos, previousGhostPos])
        possibleDirSet = Actions.getLegalNeighbors(currentGhostPos, self.map.walls) - visited

        while(len(possibleDirSet)==1):
            possibleDir = next(iter(possibleDirSet))
            visited.add(possibleDir)
            expectedPath.append(possibleDir)
            possibleDirSet = Actions.getLegalNeighbors(possibleDir, self.map.walls) - visited
        return expectedPath

    def calculateClosestDecisionPoints(self, pacmanPos):
        neighbourgs = Actions.getLegalNeighbors(pacmanPos, self.map.pacmanwalls)
        visited = set([pacmanPos])
        decisionPoints = []
        for neigbourg in neighbourgs:
            currentPath = []
            currentPath.append(neigbourg)
            visited.add(neigbourg)
            possibleDir = neigbourg
            possibleDirSet = Actions.getLegalNeighbors(neigbourg, self.map.pacmanwalls) - visited
            dist = 1
            while (len(possibleDirSet) == 1):
                dist+=1
                possibleDir = next(iter(possibleDirSet))
                currentPath.append(neigbourg)
                visited.add(possibleDir)
                possibleDirSet = Actions.getLegalNeighbors(possibleDir, self.map.pacmanwalls) - visited
            decisionPoints.append(DistancePositionPath(distance=dist, position=possibleDir, path = currentPath))
        return decisionPoints

    def getPacmanHistory(self, agentIndex=0):
        return self.pacManPosHistory[0]

    def closest_dot_distances(self, cur_pos, food_pos):
        food_distances = [manhattanDistance(food, cur_pos) for food in food_pos]
        food_distances.sort()
        return food_distances

    def closest_dot_distances_with_food_pos(self, cur_pos, food_pos):
        return [DistanceWithPosition(distance=manhattanDistance(food, cur_pos), position=food) for food in food_pos]

    def closest_ghost_positions_for_pacman(self, cur_pos, ghosts):
        ghost_distances = [GhostTuple(agentIndex=key, distance=manhattanDistance(ghost.getPosition(), cur_pos), eatable=ghost.eatable,position=ghost.getPosition()) for key, ghost in ghosts.items()]
        ghost_distances.sort(key=lambda tup: tup.distance)
        return ghost_distances

    def closest_ghost_positions(self, cur_pos, ghosts):
        ghost_distances = [GhostTuple(agentIndex=key, distance=self.calculateGhostDistance(key, cur_pos), eatable=ghost.eatable,position=ghost.getPosition()) for key, ghost in ghosts.items()]
        ghost_distances.sort(key=lambda tup: tup.distance)
        return ghost_distances

    def food_stuff(self, cur_pos, food_positions):
        food_distances = [manhattanDistance(food, cur_pos) for food in food_positions]
        return sum(food_distances)

    def cloeset_capsules(self, cur_pos, capsule_positions):
        cap_distances = [manhattanDistance(cap, cur_pos) for cap in capsule_positions]
        return min(cap_distances) if len(cap_distances) > 0 else 1

    def tunedEvaulationFunction(self,currentGameState, action):


        map_width = currentGameState.getMapWidth()
        map_height = currentGameState.getMapHeight()
        pacman_pos = currentGameState.getPacmanPosition()

        score = currentGameState.getScore()
        food = currentGameState.getFood().asList()
        ghosts = currentGameState.getGhostStates()

        closest_ghosts = self.closest_ghost_positions(pacman_pos, ghosts)
        
        cap_distances = [manhattanDistance(cap, pacman_pos) for cap in currentGameState.getCapsules()]
        closest_capsule = min(cap_distances) if len(cap_distances) > 0 else 1

        closest_dot_dist = sorted([manhattanDistance(f, pacman_pos) for f in food])
        ghost_positions = currentGameState.getGhostPositions()

        if pacman_pos in ghost_positions:
            score -= 1000


        possibleTrapScore = 0
        # decision point are the safest point -> no reason to calculate trap points
        if pacman_pos not in decision_points:
            for distance, point, path in self.calculateClosestDecisionPoints(pacman_pos):
                pacmanPathSet = set(path)
                for ghost in self.closest_ghost_positions(point, ghosts):
                    #self.gameLogCache.append("decision_point {0}, pacmanPos {1}, pacmanDistance {2}, ghostPos {3}, ghostDistance {4} \n".format(point, pacman_pos, distance, ghost.position, ghost.distance))
                    #we calculate for the next round, where the ghost has already moved
                    if (ghost.distance-1) <= distance:
                        possibleTrapScore+=15
                    ghostpath = self.calculateMandatoryGhostPath(ghost.agentIndex)
                    commonPointsInPaths = pacmanPathSet.intersection(set(ghostpath))
                    if len(commonPointsInPaths)>0 and ghostpath[-1] != point:
                        possibleTrapScore+=15
        ghostScore = 0.0
        delta = 0.9


        for i in range(len(ghosts)):
            ghostScore+=float(closest_ghosts[i].distance)*delta
            delta*=0.9
        #food_stuff_score / len(food)
        penaltyForTheSameAction = 0
        #checking last move
        if len(self.getPacmanHistory())>2 and pacman_pos == self.getPacmanHistory()[-2]:
            self.sameActionInRow += 1
            penaltyForTheSameAction = 2^self.sameActionInRow
        else:
            self.sameActionInRow=0

        closest_capsule_score = 0

        if currentGameState.getCurrentTick()<160:
            #closest_capsule*=-1
            closest_capsule_score = -50 - 15

        first_closest_dot_dist = closest_dot_dist[0] if closest_dot_dist else None
        if first_closest_dot_dist is None:
            return MY_INFINITY #WON
        second_closest_dot_dist=0
        if len(closest_dot_dist)>1:
            second_closest_dot_dist = closest_dot_dist[1]
        closest_dot_positions = [DistanceWithPosition(distance=manhattanDistance(f, pacman_pos), position=f) for f in food]

        # check if ghost in the path:
        first_closest_dot_dist = min([0 if pos in shortestPath(pacman_pos, closest_dot_positions[0].position) else first_closest_dot_dist for pos in ghost_positions])

        if len(closest_dot_positions)>1:
            second_closest_dot_dist = min([0 if pos in shortestPath(pacman_pos, closest_dot_positions[1].position) else second_closest_dot_dist for pos in ghost_positions])

        if currentGameState.getPacmanState(0).getFastTime()>0:
            sc = score - possibleTrapScore + float(ghostScore - first_closest_dot_dist - second_closest_dot_dist - penaltyForTheSameAction) / float(map_width + map_height)
        else:
            sc = score - possibleTrapScore + closest_capsule_score +  float(ghostScore - closest_capsule * 2 - first_closest_dot_dist - second_closest_dot_dist - penaltyForTheSameAction) / float(map_width + map_height)

#        self.gameLogCache.append("closest_capsule = {} closest dot 1 {} closest dot 2 {}\n".format(closest_capsule, first_closest_dot_dist, second_closest_dot_dist))
        if(closest_ghosts[0].distance<=1):
#            self.gameLogCache.append("closest ghosts => {}\n".format(str(closest_ghosts)))
            if closest_ghosts[0].eatable==0:
                sc-=490
#        self.gameLogCache.append("{0} [score = {1} gamescore = {2} ghost_score = {3} closest_dots = {4} closest_capsule = {5} trap_score = {6}, pacman_pos = {7}]\n".format(action, sc, score, ghostScore, closest_dot_positions[0].distance, closest_capsule, possibleTrapScore, pacman_pos))
#        self.gameLogCache.append("++ ghost positions: {0}\n".format(str(ghost_positions)))
        return sc

    def read_data(self):
        # stdin-t olvassuk :)
        first_line_data = list(map(int, sys.stdin.readline().strip().split(" ")))

        inputData = InputGameData(gameid=first_line_data[0], tick=first_line_data[1], pacmanid=first_line_data[2])

        second_line_data = sys.stdin.readline().strip().split(" ", 4)

        mapData = InputMapData(height=int(second_line_data[0]), width=int(second_line_data[1]), num_pacman=int(second_line_data[2]), num_ghost=int(second_line_data[3]))

        if len(second_line_data) > 4:
            sys.stderr.write("\nGot message: %s\n" % second_line_data[4])

        # TODO azert ennel kenyelmesebben is el lehet menteni az adatokat :)
        fields = []
        for i in range(mapData.height):
            fields.append(list(sys.stdin.readline())[:mapData.width])

        pacman_info = []
        for i in range(mapData.num_pacman):
            pacman_info.append(sys.stdin.readline().strip().split(" "))

        ghost_info = []
        for i in range(mapData.num_ghost):
            ghost_info.append(sys.stdin.readline().strip().split(" "))

        return (inputData, mapData, fields, pacman_info, ghost_info)

    def followPath(self, pacman_pos, path):
        #12, 24
        #12, 0      >
        first_vec = (pacman_pos[0] - path[1][0], pacman_pos[1] - path[1][1])
        first_vec = self.reverseDirectionOnEdges(first_vec)

        act1 = Actions.vectorToDirection(first_vec)
        if (len(path) > 2):
            second_vec = (path[1][0] - path[2][0], path[1][1] - path[2][1])
            second_vec = self.reverseDirectionOnEdges(second_vec)
            act2 = Actions.vectorToDirection(second_vec)
            return [act1, act2]
        else:
            return [act1]

    def reverseDirectionOnEdges(self, first_vec):
        if first_vec[0] >= 2:
            first_vec[0] = -1
        elif first_vec[0] <= -2:
            first_vec[0] = 1
        if first_vec[1] >= 2:
            first_vec[1] = -1
        elif first_vec[0] <= -2:
            first_vec[1] = 1
        return first_vec

    def doTwoActions(self, gameState, legal, data):
        first_step = self.getActionFromSuccessor(gameState, legal)
        after_first_state = first_step.state
        legal2 = after_first_state.getLegalPacmanActions()
        second_step = self.getActionFromSuccessor(after_first_state, legal2)
        # sys.stderr.write("%s %s %s %c %c\n" % (data[0], data[1], data[2], first_step.action,second_step.action))
        return OutputAction(gameid=data[0], tick=data[1], pacmanid=data[2], dir1=first_step.action, dir2=second_step.action)

    def doActions(self, gameState, data):
        # Generate candidate actions
        legal = gameState.getLegalPacmanActions()
        fast_time = gameState.getPacmanState(0).getFastTime()
        pacman_pos = gameState.getPacmanPosition()
        self.gameLogCache.append("pacmanpos: %s\n" % (str(pacman_pos)))

        closestGhosts = self.closest_ghost_positions_for_pacman(pacman_pos, gameState.getGhostStates())
        #HUNTING TIME
        #sys.stderr.write("closestGhosts %s\n" % str(closestGhosts))
        #sys.stderr.write("fasttime %d eatable %d\n" % (fast_time[0], closestGhost[1]))
        if fast_time > 0:
            nonEatablePos = []
            for closestGhost in closestGhosts:
                if closestGhost.eatable==0:
                    nonEatablePos.append(closestGhost.position)
                    continue
                pathToGhost = shortestPath(pacman_pos, closestGhost.position)
                #cannot reach unless collide
                reachable = True
                for nonEatable in nonEatablePos:
                    if nonEatable in pathToGhost:
                        reachable = False
                        break
                if reachable==False:
                    continue

                self.gameLogCache.append("nonEatablePos: %s\n" % (str(nonEatablePos)))
                self.gameLogCache.append("pathToGhost: %s\n" % (str(pathToGhost)))
                actions = self.followPath(pacman_pos, pathToGhost)
                if len(actions)==2:
                    return OutputAction(gameid=data[0], tick=data[1], pacmanid=data[2], dir1=actions[0], dir2=actions[1])
                else:
                    return OutputAction(gameid=data[0], tick=data[1], pacmanid=data[2], dir1=actions[0], dir2=None)

            return self.doTwoActions(gameState, legal, data)
        else:
            score_action_state = self.getActionFromSuccessor(gameState, legal)
            #sys.stderr.write("%s %s %s %c\n" % (data[0], data[1], data[2], score_action_state.action))
            #sys.stdout.write("%s %s %s %c\n" % (data[0], data[1], data[2], score_action_state.action))
            return OutputAction(gameid=data[0], tick=data[1], pacmanid=data[2], dir1=score_action_state.action, dir2=None)

    def getActionFromSuccessor(self, gameState, legal):
        
        def max_agent(state, depth, alpha, beta):
            if state.isWin() or state.isLose():
                return state.getScore()
            actions = state.getLegalActions(0)
            best_score = float("-inf")
            score = best_score
            best_action = actions[0]
            for action in actions:
                score = min_agent(state.generateSuccessor(0, action), depth, 'a', alpha, beta)
                if score > best_score:
                    best_score = score
                    best_action = action
                alpha = max(alpha, best_score)
                if best_score > beta:
                    return best_score
            if depth == 0:
                return ScoreActionState(score=best_score, action=best_action, state=state)
            else:
                return best_score
            
            
        def min_agent(state, depth, ghost, alpha, beta):
            if state.isLose() or state.isWin():
                return state.getScore()
            if ghost == 'a':
                next_ghost = 'b'
            elif ghost == 'b':
                next_ghost = 'c'
            elif ghost == 'c':
                next_ghost = 'd'
            if ghost == 'd':
                # Although I call this variable next_ghost, at this point we are referring to a pacman agent.
                # I never changed the variable name and now I feel bad. That's why I am writing this guilty comment :(
                next_ghost = 0
            actions = state.getLegalActions(ghost)
            best_score = float("inf")
            score = best_score
            for action in actions:
                if next_ghost == 0: # We are on the last ghost and it will be Pacman's turn next.
                    if depth == self.depth - 1:
                        score = self.tunedEvaulationFunction(state.generateSuccessor(ghost, action), action)
                    else:
                        score = max_agent(state.generateSuccessor(ghost, action), depth + 1, alpha, beta)
                else:
                    score = min_agent(state.generateSuccessor(ghost, action), depth, next_ghost, alpha, beta)
                if score < best_score:
                    best_score = score
                beta = min(beta, best_score)
                if best_score < alpha:
                    return best_score
            return best_score
        
#        successors = [(gameState.generateSuccessor(0, action), action) for action in legal]
#        scored = [ScoreActionState(score=self.tunedEvaulationFunction(state, action), action=action, state=state) for state, action in successors]
#        bestScore = max(scored).score
#        bestActions = [tup for tup in scored if tup[0] == bestScore]
        return max_agent(gameState, 0, float('-inf'), float('inf'))

    def actionLoop(self):
        pool = ThreadPool()
        while True:
            try:
                data, mapData, fields, pacmans, ghosts = self.read_data()
                game_state = self.loadCurrentGameState(pacmans, ghosts, mapData, fields, data[1])

                legal = game_state.getLegalPacmanActions()

                self.gameLogCache.append("round number: {}\n".format(data[1]))

                try:
                    async_result = pool.apply_async(self.doActions, (game_state, data))
                    outputAction = async_result.get(timeout=1.5)
                except:
                    self.gameLogCache("Timeout exception happened")
                    outputAction = OutputAction(data[0], data[1], data[2], legal[0])
                #outputAction = self.doActions(game_state, data)
            except:
                for i in reversed(range(1, 800)):
                    sys.stderr.write(self.gameLogCache[i*-1])
            if outputAction.dir2 != None:
                sys.stdout.write("%s %s %s %c %c\n" % (outputAction.gameid, outputAction.tick, outputAction.pacmanid, outputAction.dir1, outputAction.dir2))
            else:
                sys.stdout.write("%s %s %s %c\n" % (outputAction.gameid, outputAction.tick, outputAction.pacmanid, outputAction.dir1))

            if data[2] == -1:
                break



    def loadCurrentGameState(self, pacmans, ghosts, mapData, fields, tick):
        game_state = GameState()

        self.map = Map(mapData.height, mapData.width, fields)

        pacmans = [PacmanFromInput(id=int(pacman[0]), teamname=pacman[1], position=(int(pacman[2]), int(pacman[3])), fasttime=int(pacman[4]), score=int(pacman[5]), extrapoints=pacman[6]) for pacman in pacmans]
        for pacman in pacmans:
            self.pacManPosHistory[pacman.id].append(pacman.position)
        ghosts = [GhostFromInput(id=ghost[0], position=(int(ghost[1]), int(ghost[2])), eatable=int(ghost[3]),stayingtime=ghost[4]) for ghost in ghosts]
        for ghost in ghosts:
            self.ghostPosHistory[ghost.id].append(ghost.position)


        game_state.initialize(self.map, pacmans, ghosts, tick)
        return game_state


if __name__ == '__main__':
    agent = EAgent()
    agent.actionLoop()
