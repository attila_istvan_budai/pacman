from game.pacman import *
from ericsson.ericssonlayout import *
import sys
from game.gameengine import Agent
from game.pacman import Directions
from ericsson.clients import *



InputMapData = namedtuple('InputMapData', ['height', 'width','num_pacman', 'num_ghost'])
GhostFromInput = namedtuple('GhostFromInput', ['id', 'position','eatable', 'stayingtime'])
PacmanFromInput = namedtuple('PacmanFromInput', ['id', 'teamname', 'position','fasttime', 'score', 'extrapoints'])

DIR_DICT = {
    '>' : 'East',
    '<' : 'West',
    'v' : 'North',
    '^' : 'South'
}

GHOST_INDICES = ['a', 'b', 'c', 'd', 'e', 'f']

class ClientAgent(Agent):
    def __init__(self):
        self.agent = EAgent()
        self.roundNumber = 0

    def getAction(self, state):
        fields = state.getFields()
        ghosts = state.getGhostStates()

        mapinfo = InputMapData(height = state.getMapHeight(), width = state.getMapWidth(), num_pacman=1, num_ghost=len(ghosts))

        ghosts_out = []
        #for ghost in ghosts:
        for i in range(len(ghosts)):
            pos = ghosts[i].configuration.getPosition()
            freezetime = 0
            gtup = (GHOST_INDICES[i], pos[1], pos[0], ghosts[i].scaredTimer,freezetime )
            ghosts_out.append(gtup)
        pacmanState = state.getPacmanState()
        pos = pacmanState.configuration.getPosition()
        pacman_out = [('0', 'team',  (pos[1]), (pos[0]),pacmanState.fastTime,state.getScore(),'0' )]
        data = (' ', self.roundNumber, ' ', ' ')
        self.roundNumber+=1
        game_state = self.agent.loadCurrentGameState(pacman_out, ghosts_out,mapinfo,fields, data[1])

        outputAction = self.agent.doActions(game_state, data)

        if outputAction.dir2 is not None:
            return [DIR_DICT[outputAction.dir1], DIR_DICT[outputAction.dir2]]

        return DIR_DICT[outputAction.dir1]


if __name__ == '__main__':
    layout = getLayout('map')
    numGames = 1
    pacman = ClientAgent()
    ghostName = 'DirectionalGhost'
    ghostType = loadAgent(ghostName, True)
    ghosts = [ghostType( i+1 ) for i in range( layout.getNumGhosts())]
    from display import graphicsDisplay

    frameTime = 0.1
    zoom = 1.0

    record = False

    display = graphicsDisplay.PacmanGraphics(zoom, frameTime=frameTime)
    runGames(layout, pacman, ghosts, display, numGames, record)